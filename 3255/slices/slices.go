// slice problems and checking arrays is equal or not

package main

import "fmt"

func main() {
	array1 := [5]int{4, 5, 6}
	var array2 = [5]int{4, 5, 6}
	if array1 == array2 {
		fmt.Println("Equal")
	} else {
		fmt.Println("Not equal")
	}
	fmt.Println(array1)

	for i := 0; i < len(array1); i++ {
		fmt.Printf("%d ", array1[i])
	}
	fmt.Println()
}
